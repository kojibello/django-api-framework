from django.forms import ValidationError
from rest_framework import serializers
from student_api.models import Student



# class StudentSerializer(serializers.Serializer):
#     id = serializers.IntegerField(read_only = True)
#     course_name =  serializers.CharField()
#     first_name = serializers.CharField()
#     last_name =  serializers.CharField()
#     gender= serializers.CharField(allow_null=True)
#     amount_paid = serializers.CharField()
#     email = serializers.EmailField()
#     registration_date =  serializers.DateTimeField(default=datetime.now().strftime("%Y-%m-%dT%H:%M:%S"))
#     student_address =  serializers.CharField(allow_null=True)


#     ## create serializer 
#     def create(self, data):
#         return Student.objects.create(**data)

#     def update(self, instance, data):
#         instance.first_name = data.get('first_name', instance.first_name)
#         instance.last_name = data.get('last_name', instance.last_name)
#         instance.registration_date = data.get('registration_date', instance.registration_date)
#         instance.course_name = data.get('course_name', instance.course_name)
#         instance.gender = data.get('gender', instance.gender)
#         instance.amount_paid = data.get('amount_paid', instance.amount_paid)
#         instance.email = data.get('email', instance.email)
#         instance.student_address = data.get('student_address', instance.student_address)

#         instance.save()
#         return instance 

class StudentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Student
        fields = "__all__"

    def validate(self, data):
        if data['amount_paid'] < 200:
            raise ValidationError(f"${data['amount_paid']} is less than the accepted min amount of $200.00")   
        return data    

