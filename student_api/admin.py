from django.contrib import admin

# Register your models here.
from student_api.models import Student  

admin.site.register(Student)