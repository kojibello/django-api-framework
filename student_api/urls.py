
from django.contrib import admin
from django.urls import path
from student_api.views import student_list, create_student, student

urlpatterns = [
    path('', create_student),
    path('list/', student_list),
    path('<int:pk>', student)
]



