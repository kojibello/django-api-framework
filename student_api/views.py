from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import status
from student_api.models import Student 
from student_api.serializer import StudentSerializer


@api_view(['GET'])
def student_list(request):
    
    student = Student.objects.all() # COMPLEX DATA 
    serializer = StudentSerializer(student, many=True)

    return Response(serializer.data)

@api_view(['POST'])
def create_student(request):
    
    serializer = StudentSerializer(data=request.data)
    if serializer.is_valid(): 
        serializer.save()
        return Response(serializer.data)
    else:
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST )    

@api_view(['PUT', 'GET', 'DELETE'])
def student(request, pk):
    try:
        student = Student.objects.get(pk=pk)
    except:
        return Response({
            'error': "student with id {} doesn't exists".format(pk)
        }, status=status.HTTP_400_BAD_REQUEST )    

    if request.method == 'GET':
        serializer = StudentSerializer(student)
        return Response(serializer.data)


    if  request.method == 'PUT':
        serializer = StudentSerializer(student, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    if request.method == 'DELETE':
        student.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
