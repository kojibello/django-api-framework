from django.db import models

# Create your models here.


class CourseName(models.Model):
    name = models.CharField(max_length=50)
    tuitor = models.CharField(max_length=50)
    registration_date = models.DateField()

class Student(models.Model):
    first_name = models.CharField(max_length=100)
    last_name =  models.CharField(max_length=100)
    description = models.IntegerField(null = True)
    registration_date = models.DateTimeField(auto_now=True) 
    course_name =  models.CharField(max_length=100)
    gender = models.CharField(max_length=100, null = True)
    amount_paid = models.DecimalField(max_digits=6, decimal_places=2)
    email = models.EmailField(max_length=100, unique = True)
    student_address = models.CharField(max_length=100, null = True)

    def __str__(self):
        return self.first_name


        

   
## /books/list    