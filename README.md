# django-api-framework  Starting Point

Source code for my Udemy course Build a [Backend REST API with Python & Django - Advanced](https://gitlab.com/kojibello/django-api-framework.git).

The course teaches how to build a fully functioning REST API using:

 - Python
 - Django / Django-REST-Framework
 - Docker / Docker-Compose
 - Test Driven Development


## Getting started

To start project, run:

```
docker-compose up
```

The API will then be available at http://127.0.0.1:8000